﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This class is for the behaviour of the audio source prefab.
/// </summary>
public class AudioSourcePrefab : MonoBehaviour
{

    public bool shouldLoop = false;

    void Update()
    {
        CheckIfFinishedPlaying();
    }

    /// <summary>
    /// Destroys the audio source if it has finished playing.
    /// </summary>
    public void CheckIfFinishedPlaying()
    {
        if (!GetComponent<AudioSource>().isPlaying)
        {
            Destroy(gameObject);
        }
    }
}
