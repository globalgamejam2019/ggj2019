﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    #region SINGLETON PATTERN
    public static SoundManager _instance;
    public static SoundManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<SoundManager>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("SoundManager");
                    _instance = container.AddComponent<SoundManager>();
                }

            }
            return _instance;
        }
    }
    #endregion

    #region variables
    public List<GameObject> ambientLevelSounds = new List<GameObject>();
    #endregion



    void Start ()
    {
		
	}
	

	void Update ()
    {
      
	}

    public void ManageCurrentAmbientAudio(int level)
    {
        if (level > ambientLevelSounds.Count)
            return;
        if (ambientLevelSounds[level] == null)
            return;
        
            ambientLevelSounds[level].SetActive(true);
            TurnOffAllOtherAmbientAudio(level);
        
        //ActivitySelectionManager.Instance.currentActivity
    }

    public void TurnOffAllOtherAmbientAudio(int level)
    {
        for (int i = 0; i < ambientLevelSounds.Count; i++)
        {
            if(i != level)
            {
                if (!ambientLevelSounds[i])
                    return;
                ambientLevelSounds[i].SetActive(false);
            }
        }
    }


    public void PlaySound()
    {

    }
}
