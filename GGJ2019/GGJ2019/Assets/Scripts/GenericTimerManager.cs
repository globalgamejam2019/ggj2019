﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


/// <summary>
/// This class is used to manage the creation of generic timers
/// </summary>
public class GenericTimerManager : MonoBehaviour
{

    #region SINGLETON PATTERN
    public static GenericTimerManager _instance;
    public static GenericTimerManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GenericTimerManager>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("GenericTimerManager");
                    _instance = container.AddComponent<GenericTimerManager>();
                }

            }
            return _instance;
        }
    }
    #endregion

    #region variables
    public GameObject genericTimerPrefab;
    public GenericTimer.TimerDelegate methodToCall;
    #endregion

    public void CreateNewTimer(float newTimerThreshold, GenericTimer.TimerDelegate methodToPassIn)
    {
        GameObject newTimer = Instantiate(genericTimerPrefab, transform);
        newTimer.GetComponent<GenericTimer>().SetupTimer(newTimerThreshold, methodToPassIn);
    }

    /// <summary>
    /// Destroys any remaining content timers.
    /// </summary>
    public void DestroyRemainingTimers()
    {
        GenericTimer[] timers = GetComponentsInChildren<GenericTimer>().ToArray();

        foreach (GenericTimer timer in timers)
        {
            Destroy(timer.gameObject);
        }
    }

    
}
