﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// This class is used to control which activity is currently active.
/// </summary>
public class ActivitySelectionManager : MonoBehaviour {

    #region variables
    public GameObject nonCurrentActivitiesParent;
    public GameObject currentActivityParent;
    public GameObject currentActivity;
    public List<GameObject> activities = new List<GameObject>();
    private KeyCode keyActivity1 = KeyCode.Alpha1;
    private KeyCode keyActivity2 = KeyCode.Alpha2;
    private KeyCode keyActivity3 = KeyCode.Alpha3;
    private KeyCode keyActivity4 = KeyCode.Alpha4;
    private KeyCode keyActivity5 = KeyCode.Alpha5;

    #endregion


    #region SINGLETON PATTERN
    public static ActivitySelectionManager _instance;
    public static ActivitySelectionManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<ActivitySelectionManager>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("ActivitySelectionManager");
                    _instance = container.AddComponent<ActivitySelectionManager>();
                }

            }
            return _instance;
        }
    }
    #endregion

    private void Start()
    {
        currentActivity = activities[0];
        MakeActivityCurrent(0);
    }

    void Update ()
    {
        ManageActivitySelection();
    }

    /// <summary>
    /// Manages the user inputs in relation to the current activity
    /// </summary>
    public void ManageActivitySelection()
    {
        if (Input.GetKeyDown(keyActivity1))
        {
            MakeActivityCurrent(0);
        }
        else if (Input.GetKeyDown(keyActivity2))
        {
            MakeActivityCurrent(1);
        }
        else if(Input.GetKeyDown(keyActivity3))
        {
            MakeActivityCurrent(2);
        }
        else if (Input.GetKeyDown(keyActivity4))
        {
            MakeActivityCurrent(3);
        }
        else if(Input.GetKeyDown(keyActivity5))
        {
            MakeActivityCurrent(4);
        }

    }

    /// <summary>
    /// Selects an activity and makes it the current one.
    /// </summary>
    public void MakeActivityCurrent(int activity)
    {
        SoundManager.Instance.ManageCurrentAmbientAudio(activity);
        LevelIconManager.Instance.HighlightActiveLevel(activity);

        if (currentActivity)
        {
            currentActivity.transform.SetParent(nonCurrentActivitiesParent.transform);
        }

        if (activities[activity])
        {
            currentActivity = activities[activity];
        }
        else
        {
            return;
        }
        currentActivity.transform.SetParent(currentActivityParent.transform); 
    }
}
