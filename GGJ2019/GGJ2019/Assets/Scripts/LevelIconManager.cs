﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class is used for managing the icons for each level.
/// </summary>
public class LevelIconManager : MonoBehaviour {

    #region SINGLETON PATTERN
    public static LevelIconManager _instance;
    public static LevelIconManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<LevelIconManager>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("LevelIconManager");
                    _instance = container.AddComponent<LevelIconManager>();
                }

            }
            return _instance;
        }
    }
    #endregion

    #region variables
    public List<GameObject> levelIcons = new List<GameObject>();
    public Color nonHighlightedColour = Color.black;
    public Color highlightedColour = Color.red;
    #endregion


    /// <summary>
    /// Highlights the active level with a coloured outline.
    /// </summary>
    /// <param name="currentLevel"></param>
    public void HighlightActiveLevel(int currentLevel)
    {
        for (int i = 0; i < levelIcons.Count; i++)
        {
            if(i == currentLevel)
            {
                levelIcons[i].transform.GetChild(0).GetComponent<Image>().color = highlightedColour;
            }
            else
            {
                levelIcons[i].transform.GetChild(0).GetComponent<Image>().color = nonHighlightedColour;
            }
        }
    }

    /// <summary>
    /// Shows the tick above the level icon, indicating that the level has been completed.
    /// </summary>
    public void TickCompleteLevel(int levelCompleted)
    {
        levelIcons[levelCompleted].transform.GetChild(1).gameObject.SetActive(true);
    }

    /// <summary>
    /// Highlights the level icon that was just clicked.
    /// </summary>
    public void HighlightClickedOnLevelIcon(int levelIconID)
    {

    }

}
