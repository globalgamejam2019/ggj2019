﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Checks to see if the game is over by winning.
/// </summary>
public class GameOverManager : MonoBehaviour {

    #region SINGLETON PATTERN
    public static GameOverManager _instance;
    public static GameOverManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameOverManager>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("GameOverManager");
                    _instance = container.AddComponent<GameOverManager>();
                }

            }
            return _instance;
        }
    }
    #endregion

    #region variables
    private float activityCompleteTally = 0;
    private float activityCompleteThreshold =4;
    [SerializeField]
    private string[] gameOverReasons2 = {"The food burned!","Too many wrong clothes sorted!","The baby died!","The bath overflowed!","The bath is too hot!","The bath is too cold!","You ran out of time!" };
    public string gameOverReason;
    public int gameOverSoundID = 0;
    public float finalTimeRemaining;
    #endregion

    private void Start()
    {
        DontDestroyOnLoad(this);
    }

    public void ActivityCompleted()
    {
        activityCompleteTally++;
        GlobalAudioManager.Instance.CreateAudioSource("DingCorrect");
        CheckIfGameWon();
    }

    /// <summary>
    /// Checks if the number of complete activities matches the threshold.
    /// </summary>
    public void CheckIfGameWon()
    {
        if(activityCompleteTally == activityCompleteThreshold)
        {
            finalTimeRemaining = GetComponent<GlobalGameTimer>().currentTime;
            SceneManager.LoadScene("WinScreen");
        }
    }

    public void LoadGameOver()
    {
        SceneManager.LoadScene("LoseScreen");
        gameObject.GetComponent<GlobalGameTimer>().enabled = false;
    }

    /// <summary>
    /// Sets up the gameover reason string.
    /// </summary>
    public void SetGameOverReason(int reason)
    {
        gameOverReason = gameOverReasons2[reason];
    }

    public void SetGameOverSound(int ID)
    {
        gameOverSoundID = ID;
    }
}
