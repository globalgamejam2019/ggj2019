﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Drag : MonoBehaviour, IDragHandler, IDropHandler {

    #region variables
    private bool isFoodOnPan;
    public Transform utensilCentre;
    public Game1Cooking game1Cooking;
    #endregion

    public void OnDrag(PointerEventData eventData)
    {
        if (game1Cooking.cookingInProgress)
            return;

        transform.position = Input.mousePosition;
    }
    

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.localPosition = Vector3.zero;  
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (isFoodOnPan)
        {
            transform.position = utensilCentre.position;
            Game1Cooking.Instance.CookSteakRawToMedium();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PAN")
        {
            isFoodOnPan = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
       // isFoodOnPan = false;
    }
}
