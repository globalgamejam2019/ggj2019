﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// This class is for managing Game 4: Saving the child
/// </summary>
public class Game1Cooking : MonoBehaviour
{

    #region SINGLETON PATTERN
    public static Game1Cooking _instance;
    public static Game1Cooking Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Game1Cooking>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("Game1Cooking");
                    _instance = container.AddComponent<Game1Cooking>();
                }

            }
            return _instance;
        }
    }
    #endregion

    #region variables
    private int levelID = 0;
    public List<StirCollider> stirColliders = new List<StirCollider>();
    private int currentCollider = 0;
    private int stirColliderThreshold;
    public StirCollider nextCollider;
    private int stirRevolutions = 0;
    private int stirRevolutionsThreshold = 1;

    public GameObject generalTimerPrefab;
    private float sauceSpinTime = 3f;
    private float burnWindow = 3f;
    public Animator sauce;

    public GameObject sauceBurnTimer;
    public GameObject steakBurnTimer;

    private int sauceGameCycles;
    private int sauceGameCyclesThreshold = 3;

    public Sprite sauceDoneSprite;

    private float steakCookTime = 3f;
    private int steakSidesDone = 0;
    private int steakSidesDoneThreshold = 2;
    private bool inSteakBurnWindow = false;
    private int steakProgress;
    public Image steak;

    public Sprite steakRawSprite;
    public Sprite steakMediumSprite;
    public Sprite steakDoneSprite;

    public GameObject flipSteakText;

    private bool activityCompletedOnce = false;

    public GameObject steakFlipSound;
    public GameObject stirPotSound;

    public GameObject cookSteakLabel;
    public GameObject stirLabel;
    public GameObject stirArrows;

    public bool cookingInProgress = false;
    #endregion

    void Start()
    {
        nextCollider = stirColliders[currentCollider];
        stirColliderThreshold = stirColliders.Count;
    }

    void Update()
    {
        ManagePlayerControls();
    }

    /// <summary>
    /// Checks to see if the activity is finished.
    /// </summary>
    public void CheckIfActivityComplete()
    {
        if (activityCompletedOnce)
            return;
        if(CheckIfSauceDone() && CheckIfSteakDone())
        {
            activityCompletedOnce = true;
            LevelIconManager.Instance.TickCompleteLevel(levelID);
            GameOverManager.Instance.ActivityCompleted();
            Debug.Log("ACTIVITY COMPLETED");
        }
        //steak must be cooked on both sides
        //sauce must be stirred
    }

    /// <summary>
    /// Manages the player controls for each game.
    /// </summary>
    public void ManagePlayerControls()
    {
        if (ActivitySelectionManager.Instance.currentActivity != gameObject)
            return;
     
        ManageActivityMovement();
    }

    public void ManageActivityMovement()
    {
        if (inSteakBurnWindow)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                CookSteakRawToMedium();
                steakFlipSound.GetComponent<FMODUnity.StudioEventEmitter>().Play();

                //HIDE FLIP TEXT
            }
        }
    }
    #region Sauce Methods
    public void EnteredStirCollider(StirCollider collider)
    {
        if (nextCollider == collider)
        {
            currentCollider++;
            if (currentCollider == stirColliderThreshold)
            {
                ResetStirRevolution();
                RevolutionComplete();
                return;
            }
            nextCollider = stirColliders[currentCollider];
        }
        else
        {
            ResetStirRevolution();
        }
        
    }

    public void ResetStirRevolution()
    {
        currentCollider = 0;
        nextCollider = stirColliders[currentCollider];
      
    }

    public void RevolutionComplete()
    {
        Debug.Log("revolution complete");
        stirRevolutions++;
        CheckIfNumberOfRevolutionsMet();
    }


    public void CheckIfNumberOfRevolutionsMet()
    {
        if(stirRevolutions == stirRevolutionsThreshold)
        {
            stirPotSound.GetComponent<FMODUnity.StudioEventEmitter>().Play();
            DisableStirPrompt();
            StartStirAnimation();
            Debug.Log("Stir revolutions Met");
        }
    }

    public void StartStirAnimation()
    {
        if (sauceBurnTimer)
        {
            Destroy(sauceBurnTimer);
            sauceBurnTimer = null;
        }

        //remove stir text
        sauceGameCycles++;
        sauce.SetBool("RotateSauce", true);
        GameObject newTimer = Instantiate(generalTimerPrefab);
        newTimer.GetComponent<GeneralTimer>().SetupTimer(sauceSpinTime, StartBurnWindow);
    }

    public void StartBurnWindow()
    {
        sauce.SetBool("RotateSauce", false);
        if (CheckIfSauceDone())
        {
            CheckIfActivityComplete();
            return;
        }
        stirRevolutions = 0;
        //Debug.Log("STIR!");
        EnableStirPrompt();
        //enable stir text!
        sauceBurnTimer = Instantiate(generalTimerPrefab);
        sauceBurnTimer.GetComponent<GeneralTimer>().SetupTimer(burnWindow, BurntAnItem);
    }

    //destroy the timer object 

    public void BurntAnItem( )
    {
        //set lose text to "you burnt the "item" "
        GameOverManager.Instance.SetGameOverReason(0);
        GameOverManager.Instance.LoadGameOver();
    }

    public bool CheckIfSauceDone()
    {
        if(sauceGameCycles == sauceGameCyclesThreshold)
        {
           // enable tick sign
            sauce.GetComponent<Image>().sprite = sauceDoneSprite;
            
            return true;
        }
        else
        {
            return false;
        }
    }

    public void EnableStirPrompt()
    {
        stirLabel.SetActive(true);
        stirArrows.SetActive(true);
    }

    public void DisableStirPrompt()
    {
        stirLabel.SetActive(false);
        stirArrows.SetActive(false);
    }

    #endregion

    #region steak Methods
    public void CookSteakRawToMedium()
    {
        if (cookingInProgress)
            return;
        else
            cookingInProgress = true;
        //should disable after dropped into pan. 
        //once it has fully cooked, then re-enable the function.
        flipSteakText.SetActive(false);
        steakFlipSound.GetComponent<FMODUnity.StudioEventEmitter>().Play();
        cookSteakLabel.SetActive(false);
        inSteakBurnWindow = false;
        steakProgress = 0;
        CheckSteakProgress();
        if (steakBurnTimer)
        {
            Destroy(steakBurnTimer);
            steakBurnTimer = null;
        }

        GameObject newTimer = Instantiate(generalTimerPrefab);
        newTimer.GetComponent<GeneralTimer>().SetupTimer(steakCookTime, CookSteakMediumToDone);
    }

    public void CookSteakMediumToDone()
    {
        steakProgress++;
        CheckSteakProgress();
        GameObject newTimer = Instantiate(generalTimerPrefab);
        newTimer.GetComponent<GeneralTimer>().SetupTimer(steakCookTime, StartSteakBurnWindow);
    }

    public void StartSteakBurnWindow()
    {
        steakProgress++;
        CheckSteakProgress();
        steakSidesDone++;
        if (CheckIfSteakDone())
        {
            CheckIfActivityComplete();
            return;
        }
        
        inSteakBurnWindow = true;
        //SHOW FLIP TEXT
        cookingInProgress = false;
        flipSteakText.SetActive(true);
        steakBurnTimer = Instantiate(generalTimerPrefab);
        steakBurnTimer.GetComponent<GeneralTimer>().SetupTimer(steakCookTime, BurntAnItem);
    }


    public bool CheckIfSteakDone()
    {
        if (steakSidesDone == steakSidesDoneThreshold)
        {
            // enable tick sign
          
            return true;
        }
        else
        {
            return false;
        }
    }

    public void CheckSteakProgress()
    {
       

        switch (steakProgress)
        {
            case 0:
                steak.sprite = steakRawSprite;
                break;
            case 1:
                steak.sprite = steakMediumSprite;

                break;
            case 2:
                steak.sprite = steakDoneSprite;
                break;
        }
    }
    #endregion

}
