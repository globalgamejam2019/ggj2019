﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class GameBaseTemplate : MonoBehaviour
{

    #region SINGLETON PATTERN
    public static Game2Washing _instance;
    public static Game2Washing Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Game2Washing>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("Game2Washing");
                    _instance = container.AddComponent<Game2Washing>();
                }

            }
            return _instance;
        }
    }
    #endregion

    #region variables

 
    #endregion

    void Start()
    {
  
    }

    void Update()
    {
        ManagePlayerControls();
    }

  
    
 

    /// <summary>
    /// Checks to see if the activity is finished.
    /// </summary>
    public void CheckIfActivityComplete()
    {
        
    }

    /// <summary>
    /// Manages the player controls for each game.
    /// </summary>
    public void ManagePlayerControls()
    {
        if (ActivitySelectionManager.Instance.currentActivity != gameObject)
            return;
     
        ManageActivityMovement();
    }

    public void ManageActivityMovement()
    {
    
    }

  
  

   

}
