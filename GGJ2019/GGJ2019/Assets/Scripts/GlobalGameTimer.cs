﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GlobalGameTimer : MonoBehaviour {


    #region variables
    public Text timerText;
    public bool startTimer = false;
    public float currentTime = 0f;
    private float gameDuration = 120f;
    Scene currentScene;
    #endregion

    void Start ()
    {
        //currentScene = SceneManager.GetActiveScene();
        //if (currentScene.name == "LoseScreen")
        //    return; 
        startTimer = true;
        currentTime = gameDuration;
    }
	
	
	void Update ()
    {
        ManageTimer();
    }

    public void ManageTimer()
    {
        if (!startTimer)
            return;

        if (currentTime > 0)
        {
            currentTime -= Time.deltaTime;

            if(timerText !=null)
             timerText.text = currentTime.ToString("#.00");
        }
        else
        {
            startTimer = false;
            currentTime = 0;
            timerText.text = "000:00";
            // LOSS reason = ran out of time
         
            GameOverManager.Instance.SetGameOverReason(6);
            GameOverManager.Instance.LoadGameOver();
           
        }
    }
}
