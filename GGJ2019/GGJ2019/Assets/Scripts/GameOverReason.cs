﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is used to set the Game Over Reason (if there is one)
/// </summary>
public class GameOverReason : MonoBehaviour {

	void Start ()
    {
        GetComponent<Text>().text = GameOverManager.Instance.gameOverReason;
        GlobalAudioManager.Instance.CreateAudioSource(GlobalAudioManager.Instance.audioSourcePrefabs[GameOverManager.Instance.gameOverSoundID].gameObject.name);
	}
	
}
