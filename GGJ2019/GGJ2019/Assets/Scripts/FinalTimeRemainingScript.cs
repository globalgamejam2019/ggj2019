﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalTimeRemainingScript : MonoBehaviour {


	void Start ()
    {
        GetComponent<Text>().text = "Time remaining : " + GameOverManager.Instance.finalTimeRemaining.ToString("#.00");
    }

}
