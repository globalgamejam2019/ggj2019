﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// This class is for managing Game 4: Saving the child
/// </summary>
public class Game3Hoovering : MonoBehaviour
{

    #region SINGLETON PATTERN
    public static Game3Hoovering _instance;
    public static Game3Hoovering Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Game3Hoovering>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("Game3Hoovering");
                    _instance = container.AddComponent<Game3Hoovering>();
                }

            }
            return _instance;
        }
    }
    #endregion

    #region variables
    private int levelID = 2;
    #region hoover variables
    private float hooverVelocity = 0f;
    private float power = .325f;
    private float friction = 0.95f;
    private float steering = 3.0f;
    private bool moveForward = false;
    private bool moveBackward = false;
    private Rigidbody2D hooverRigidBody;
    public Transform spawnPoint;
    #endregion
    #region dirt variables
    public GameObject dirtPatchParent;
    public List<GameObject> dirtPatches = new List<GameObject>();
    #endregion

    public GameObject hooverSuck;
    #endregion

    void Start()
    {
        hooverRigidBody = GetComponent<Rigidbody2D>();

       foreach(Transform child in dirtPatchParent.transform)
        {
            dirtPatches.Add(child.gameObject);
        }
    }

    void Update()
    {
        ManagePlayerControls();
    }

    /// <summary>
    /// Checks to see if the activity is finished.
    /// </summary>
    public void CheckIfActivityComplete()
    {
        if(dirtPatches.Count == 0)
        {
            LevelIconManager.Instance.TickCompleteLevel(levelID);
            GameOverManager.Instance.ActivityCompleted();            
        }
    }

    /// <summary>
    /// Manages the player controls for each game.
    /// </summary>
    public void ManagePlayerControls()
    {
        if (ActivitySelectionManager.Instance.currentActivity != transform.parent.gameObject)
        {
            return;
        }

        ManageHooverMovement();
    }

    public void ManageHooverMovement()
    {
        float turn = Input.GetAxis("Horizontal") * -steering;

        if (Input.GetKeyDown(KeyCode.UpArrow)|| Input.GetKeyDown(KeyCode.W))
        {
            moveForward = true;
        }
        if (Input.GetKeyUp(KeyCode.UpArrow)|| Input.GetKeyUp(KeyCode.W))
        {
            moveForward = false;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            moveBackward = true;
        }
        if (Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.S))
        {
            moveBackward = false;
        }
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, 0, turn);
        }

        hooverVelocity *= friction;
        transform.Translate(Vector2.right * -hooverVelocity);
    }

        void FixedUpdate()
        {
            ManageHooverPower();
        }

    /// <summary>
    /// Manages the hoover power
    /// </summary>
    public void ManageHooverPower()
    {
        if (moveForward)
        {
            hooverVelocity -= power;
        }
        if (moveBackward)
        {
            hooverVelocity += power;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "DIRTPATCH")
        {
            HooverCollidedWithDirt(collision);
        }    

        if(collision.tag == "OUTOFBOUNDS")
        {
            TeleportHooverToSpawnPoint();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "OBSTACLE")
        {
            float currentVelocity = hooverVelocity;
            hooverVelocity = 0;
            
            hooverRigidBody.velocity = Vector2.zero;
            //bounce the hoover backwards      
            transform.Translate(Vector2.right * currentVelocity * 10);

        }
    }

    /// <summary>
    /// Called when the hoover collides with a patch of dirt.
    /// </summary>
    public void HooverCollidedWithDirt(Collider2D collision)
    {
        hooverSuck.GetComponent<FMODUnity.StudioEventEmitter>().Play();
        dirtPatches.Remove(collision.gameObject);
        Destroy(collision.gameObject);
        CheckIfActivityComplete();
    }


    public void TeleportHooverToSpawnPoint()
    {
        Debug.Log("Called");
        hooverRigidBody.velocity = Vector2.zero;
        transform.position = spawnPoint.position;
    }

}
