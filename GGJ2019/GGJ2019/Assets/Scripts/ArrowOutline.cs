﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowOutline : MonoBehaviour {

    

    private void OnEnable()
    {
        GenericTimerManager.Instance.CreateNewTimer(0.1f, DisableObject);
    }

    private void DisableObject()
    {
        gameObject.SetActive(false);
    }
}
