﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// This class is for managing the audio of all the different content scenarios of the application.
/// </summary>
public class GlobalAudioManager : MonoBehaviour
{
    #region SINGLETON PATTERN
    public static GlobalAudioManager _instance;
    public static GlobalAudioManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GlobalAudioManager>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("GlobalAudioManager");
                    _instance = container.AddComponent<GlobalAudioManager>();
                }

            }
            return _instance;
        }
    }
    #endregion

    #region variables
    public AudioMixer maritimeARAudioMixer;
    public GameObject[] audioSourcePrefabs;
    public AudioMixerGroup[] maritimeARAudioMixerGroups;
    


    private float maxMyValue =0;
    private float minMyValue = -80;
    private float myValue =0 ; // the total
    private float changePerSecond; // modify the total, every second
    private float fadeTime = 1f; // the total time myValue will take to go from max to min
    private List<GameObject> currentContentAudioSources = new List<GameObject>();
    #endregion

    void Start()
    {
        changePerSecond  = (minMyValue - maxMyValue) / fadeTime;
        LowerAllContentAudioGroups();
    }

    /// <summary>
    /// Sets the volume of all the content audio mixer groups to the minimum, in preparation for fading in.
    /// </summary>
    public void LowerAllContentAudioGroups()
    {
        for(int i = 0; i < maritimeARAudioMixerGroups.Length; i++)
        {
            string volumeParameterName = "contentVol" + (i + 1).ToString();
            maritimeARAudioMixer.SetFloat(volumeParameterName, minMyValue);
        }
    }


    /// <summary>
    /// Creates a new audio source from the available audio source prefabs.
    /// </summary>
    /// <param name="audioName"></param>
    public void CreateAudioSource(string audioName)
    {
        foreach(GameObject audioSource in audioSourcePrefabs)
        {
            if(audioName == audioSource.name)
            {
                GameObject newAudioSource = Instantiate(audioSource, transform);
                currentContentAudioSources.Add(newAudioSource);
            }
        }
    }

    /// <summary>
    /// Fades the content audio group in.
    /// </summary>
    /// <param name="targetAudioGroup"></param>
    /// <returns></returns>
    public IEnumerator FadeAudioGroupIn(int targetAudioGroup)
    {
        string volumeParameterName = "contentVol" + (targetAudioGroup + 1).ToString();
        maritimeARAudioMixer.GetFloat(volumeParameterName, out myValue);
        while (myValue < maxMyValue)
        {
            myValue = Mathf.Clamp(myValue - changePerSecond * Time.deltaTime, minMyValue, maxMyValue);
            maritimeARAudioMixer.SetFloat(volumeParameterName, myValue);

            yield return null;
        }
    }

    /// <summary>
    /// Fades the content audio group out.
    /// </summary>
    /// <param name="targetAudioGroup"></param>
    /// <returns></returns>
    public IEnumerator FadeAudioGroupOut(int targetAudioGroup)
    {
        string volumeParameterName = "contentVol" + (targetAudioGroup + 1).ToString();
        maritimeARAudioMixer.GetFloat(volumeParameterName, out myValue);

        while (myValue > minMyValue)
        {
            myValue = Mathf.Clamp(myValue + changePerSecond * Time.deltaTime, minMyValue, maxMyValue);
            maritimeARAudioMixer.SetFloat(volumeParameterName, myValue);

            yield return null;
        }
        DestroyPreviousContentAudioSources();
    }

    /// <summary>
    /// Destroys all the audio sources relating to the content that just finished.
    /// </summary>
    public void DestroyPreviousContentAudioSources()
    {
        foreach (GameObject audioSource in currentContentAudioSources)
        {
            Destroy(audioSource);
        }
        currentContentAudioSources.Clear();
    }

}
