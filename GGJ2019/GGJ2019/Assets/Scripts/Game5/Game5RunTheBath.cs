﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


public class Game5RunTheBath : MonoBehaviour
{

    #region SINGLETON PATTERN
    public static Game5RunTheBath _instance;
    public static Game5RunTheBath Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Game5RunTheBath>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("Game5RunTheBath");
                    _instance = container.AddComponent<Game5RunTheBath>();
                }

            }
            return _instance;
        }
    }
    #endregion

    #region variables
    private int levelID = 4;
    private int hotTapTurnTally;
    private int coldTapTurnTally;
    public Image bathWater;
    private float baseSpeed = 120f;
    private float currentHotFlowSpeed = 0;
    private float currentColdFlowSpeed = 0;
    private float coldWaterTemperature = 5f;
    private float hotWaterTemperature = 46f;
    private float perfectTemperatureLowerLimit = 35;
    private float perfectTemperatureUpperLimit = 37;
    private float flowSpeed1 = 1f;
    private float flowSpeed2 = 1.5f;
    private float flowSpeed3 = 2f;
    private float hotWaterAmount;
    private float coldWaterAmount;
    private float totalWaterAmount;
    private float coldWaterRatio;
    private float hotWaterRatio;
    private float currentBathTemperature;
    private float minBathFill = .833f;
    private float maxBathFill = .934f;

    private Color32 blueColour = new Color32(0, 121, 255, 255);
    private Color32 greenColour = new Color32(0, 255, 21, 255);
    private Color32 redColour = new Color32(255, 12, 0, 255);
    private Color32 currentColour = new Color32(0, 0, 0, 255);
    private Color32 yellowColour = new Color32(255, 255, 0, 255);
    public Button coldTapOn;
    public Button coldTapOff;
    public Button hotTapOn;
    public Button hotTapOff;
    public Text temperatureText;
    private bool gameCompleted = false;
    public GameObject turnTapOffSound;
    public GameObject turnTapOnSound;
    public GameObject coldWaterFlowImage;
    public GameObject hotWaterFlowImage;

    public GameObject waterTrickleSound;
    private bool minFillLineReached = false;
    public GameObject coldWaterSound;
    public GameObject hotWaterSound;
    private bool hotSoundPlayed = false;
    private bool coldSoundPlayed = false;

    #endregion

    #region workings
    //fade between each colour depending on current temperature
    //minimum temp = 5, max temp = 46
    //cold tap comes out at 5
    //hot tap comes out at 46
    //perfect temp = 35-37
    //fade between
    //minimum fill = .833
    //max fill = .934

    //need fill amounts for each temperature
    //work out proportion of hot vs cold by doing fill amount hot/totalfillamount and same for cold
    //need total fill amount for the bath.
    //add the proportion of cold  to hot to get the current temperature

    //80 % of the water is hot = 46
    //20 % of the water is cold = 5

    //1 would be the number
    //so .8 of 46 would be 36.8
    //.2 of 5 would be 1
    //36.8 + 1 = 37.8

    //you've got R G B

    //blue ->green for R = 0 -> 0
    //blue ->green for G = 121 -> 255
    //blue ->green for B = 255 -> 21

    //will move from 0 = 1
    // min =  121 max = 255
    //same ratio as

    //255 - 121 =134 is the total to move


    //121 + (134 * (currentBathTemperature/perfectTemperatureLowerLimit))
    //max - min + calculated amount
    #endregion



    void Update()
    {
        ManagePlayerControls();
        CalculateCurrentTemperature();
        ManageTemperatureTextColour();
        ManageBathVisualFill();
        CheckForGameOver();
    }


    /// <summary>
    /// Calculates the color of the temperature text.
    /// </summary>
    /// <param name="startColor"></param>
    /// <param name="endColor"></param>
    /// <param name="startTemperatureLimit"></param>
    /// <param name="endTemperatureLimit"></param>
    public void ManageTemperatureRangeColour(Color32 startColor, Color32 endColor, float startTemperatureLimit, float endTemperatureLimit)
    {
        float transitionLength = endTemperatureLimit - startTemperatureLimit;
        float progress = (currentBathTemperature - startTemperatureLimit) / transitionLength;

        float newR = startColor.r + ((endColor.r - startColor.r) * progress);
        float newG = startColor.g + ((endColor.g - startColor.g) * progress);
        float newB = startColor.b + ((endColor.b - startColor.b) * progress);
        currentColour = new Color32((byte)(Mathf.Round(newR)), (byte)(Mathf.Round(newG)), (byte)(Mathf.Round(newB)), 255);
    }




    /// <summary>
    /// Calculates the current temperature of the water in the bath.
    /// </summary>
    public void CalculateCurrentTemperature()
    {
        totalWaterAmount = hotWaterAmount + coldWaterAmount;
        coldWaterRatio = coldWaterAmount / totalWaterAmount;
        hotWaterRatio = hotWaterAmount / totalWaterAmount;

        currentBathTemperature = (hotWaterRatio * hotWaterTemperature) + (coldWaterRatio * coldWaterTemperature);
        
        if (float.IsNaN(currentBathTemperature))
            return;

        // string[] bathTempStrings = currentBathTemperature.ToString().Split(new char[] { '.' });
        //temperatureText.text = bathTempStrings[0];
        temperatureText.text = currentBathTemperature.ToString("F1");
    }

    //if it gets hot, then play sound, turn on soundplayed bool, only turns off when goes back below temp

    /// <summary>
    /// Manages the colour of the temperature text.
    /// </summary>
    public void ManageTemperatureTextColour()
    {
        //too cold
        if (currentBathTemperature >= coldWaterTemperature && currentBathTemperature < perfectTemperatureLowerLimit)
        {
            if (!coldSoundPlayed)
            {
                coldWaterSound.GetComponent<FMODUnity.StudioEventEmitter>().Play();
                coldSoundPlayed = true;
            }
            ManageTemperatureRangeColour(blueColour, greenColour, coldWaterTemperature, perfectTemperatureLowerLimit);
            temperatureText.color = currentColour;
        }
        //just right
        else if (currentBathTemperature >= perfectTemperatureLowerLimit && currentBathTemperature < perfectTemperatureUpperLimit)
        {
            hotSoundPlayed = false;
            coldSoundPlayed = false;
            temperatureText.color = yellowColour;
        }
        //too hot
        else if  (currentBathTemperature > perfectTemperatureUpperLimit +0.1f && currentBathTemperature <= hotWaterTemperature)
        {
            if(!hotSoundPlayed)
            {
                hotWaterSound.GetComponent<FMODUnity.StudioEventEmitter>().Play();
                hotSoundPlayed = true;
            }
            ManageTemperatureRangeColour(greenColour, redColour, perfectTemperatureUpperLimit, hotWaterTemperature);
            temperatureText.color = currentColour;
        }
    }

    /// <summary>
    /// Manages the filling of the bath in relation to the flow speeds.
    /// </summary>
    public void ManageBathVisualFill()
    {
        
        if (currentColdFlowSpeed == 0)
        {
            bathWater.fillAmount += 1 / ( baseSpeed / currentHotFlowSpeed ) * Time.deltaTime;
            hotWaterAmount +=  currentHotFlowSpeed * Time.deltaTime;
        }
        else if (currentHotFlowSpeed == 0)
        {
            bathWater.fillAmount += 1 / ( baseSpeed / currentColdFlowSpeed ) * Time.deltaTime;
            coldWaterAmount += currentColdFlowSpeed * Time.deltaTime;
        }
        else if (currentHotFlowSpeed != 0 && currentColdFlowSpeed != 0)
        {
            bathWater.fillAmount += 1 / (baseSpeed / (currentHotFlowSpeed + currentColdFlowSpeed)) * Time.deltaTime;
            hotWaterAmount += currentHotFlowSpeed * Time.deltaTime;
            coldWaterAmount += currentColdFlowSpeed * Time.deltaTime;
        }
    }

    /// <summary>
    /// Turns on the tap by one twist.
    /// </summary>
    /// <param name="tap"> 0 = hot tap, 1 = cold tap</param>
    public void TurnOnTap(int tap)
    {
        switch (tap)
        {
            case 0:
                if (hotTapTurnTally < 3)
                {
                    hotWaterFlowImage.SetActive(true);
                    hotTapTurnTally++;
                    turnTapOnSound.GetComponent<FMODUnity.StudioEventEmitter>().Play();
                }
                    ChangeTapFlow(tap);
                break;
            case 1:
                if (coldTapTurnTally < 3)
                {
                    coldWaterFlowImage.SetActive(true);
                    coldTapTurnTally++;
                    turnTapOnSound.GetComponent<FMODUnity.StudioEventEmitter>().Play();
                }
                ChangeTapFlow(tap);
                break;
        }
    }

    /// <summary>
    /// Turns off the tap by one twist.
    /// </summary>
    /// <param name="tap"> 0 = hot tap, 1 = cold tap</param>
    public void TurnOffTap(int tap)
    {
        switch (tap)
        {
            case 0:
                if (hotTapTurnTally > 0)
                {
                    turnTapOffSound.GetComponent<FMODUnity.StudioEventEmitter>().Play();
                    hotTapTurnTally--;
                    if (hotTapTurnTally == 0)
                        hotWaterFlowImage.SetActive(false);
                }
                    ChangeTapFlow(tap);
                break;
            case 1:
                if (coldTapTurnTally > 0)
                {
                    turnTapOffSound.GetComponent<FMODUnity.StudioEventEmitter>().Play();
                    coldTapTurnTally--;
                    if (coldTapTurnTally == 0)
                        coldWaterFlowImage.SetActive(false);
                }
                    ChangeTapFlow(tap);
                break;
        }
    }


    /// <summary>
    /// Changes the tap flow speed in relation to how many times the tap has been turned.
    /// </summary>
    /// <param name="tap"></param>
    public void ChangeTapFlow(int tap)
    {
        switch (tap)
        {
            case 0:
                switch (hotTapTurnTally) {
                    case 0:
                        currentHotFlowSpeed = 0;
                    break;
                    case 1:
                        currentHotFlowSpeed = flowSpeed1;
                        break;
                    case 2:
                        currentHotFlowSpeed = flowSpeed2;
                        break;
                    case 3:
                        currentHotFlowSpeed = flowSpeed3;
                        break;
                }

                break;
            case 1:
                switch (coldTapTurnTally)
                {
                    case 0:
                        currentColdFlowSpeed = 0;
                        break;
                    case 1:
                        currentColdFlowSpeed = flowSpeed1;
                        break;
                    case 2:
                        currentColdFlowSpeed = flowSpeed2;
                        break;
                    case 3:
                        currentColdFlowSpeed = flowSpeed3;
                        break;
                }
                break;
        }
    }


    /// <summary>
    /// Checks to see if there should be a game over or a success.
    /// </summary>
    public void CheckForGameOver()
    {
        if (gameCompleted)
            return;

        //If the bath overflows
        if (bathWater.fillAmount >= maxBathFill)
        {
            GameOverManager.Instance.SetGameOverSound(2);
            GameOverManager.Instance.SetGameOverReason(3);
            GameOverManager.Instance.LoadGameOver();
        }

        //if the fill amount is within the finish guidelines.
        if (bathWater.fillAmount >= minBathFill && bathWater.fillAmount < maxBathFill)
        {
            WaterPassedFirstFillLine();
            //if both taps have been stopped
            if (currentColdFlowSpeed == 0 & currentHotFlowSpeed == 0)
            {
                //if the temperature is perfect
                if( currentBathTemperature >= perfectTemperatureLowerLimit && currentBathTemperature <= perfectTemperatureUpperLimit)
                {
                    LevelIconManager.Instance.TickCompleteLevel(levelID);
                    GameOverManager.Instance.ActivityCompleted();
                    waterTrickleSound.SetActive(false);

                    gameCompleted = true;
                }
                else if(currentBathTemperature < perfectTemperatureLowerLimit)
                {
                    GameOverManager.Instance.SetGameOverReason(5);
                    GameOverManager.Instance.LoadGameOver();
                }
                else if (currentBathTemperature > perfectTemperatureUpperLimit)
                {
                    GameOverManager.Instance.SetGameOverReason(4);
                    GameOverManager.Instance.LoadGameOver();
                }
                else 
                {
                    GameOverManager.Instance.LoadGameOver();
                }
            }
        }
    }

    /// <summary>
    /// Manages the player controls for each game.
    /// </summary>
    public void ManagePlayerControls()
    {
        if (ActivitySelectionManager.Instance.currentActivity != gameObject)
        {
            SetTapControlsInteractable(false);

        }
        else
        {
            SetTapControlsInteractable(true);
        }
    }

    public void SetTapControlsInteractable (bool isEnabled)
    {
        coldTapOn.interactable = isEnabled;
        coldTapOff.interactable = isEnabled;
        hotTapOn.interactable = isEnabled;
        hotTapOff.interactable = isEnabled;
    }

    public void WaterPassedFirstFillLine()
    {
        if (!minFillLineReached)
        {
            waterTrickleSound.SetActive(true);
            minFillLineReached = true;
        }
    }

 


  
  

   

}
