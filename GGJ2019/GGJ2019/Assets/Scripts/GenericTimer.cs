﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to create a generic timer that will call a specified function after the specified time is up.
/// </summary>
public class GenericTimer : MonoBehaviour
{

    public float currentTime = 0f;
    public float timerThreshold = 0f;
    public bool startTimer = false;


    public delegate void TimerDelegate();
    public TimerDelegate methodToCall;

    public void SetupTimer(float newTimerThreshold, TimerDelegate methodToPassIn)
    {
        timerThreshold = newTimerThreshold;
        methodToCall = methodToPassIn;
        startTimer = true;
    }



    private void Update()
    {
        ManageTimer();
    }

    public void ManageTimer()
    {
        if (!startTimer)
            return;

        if (currentTime < timerThreshold)
        {
            currentTime += Time.deltaTime;
        }
        else
        {
            currentTime = 0;
            startTimer = false;
            methodToCall();
            Destroy(gameObject);
        }
    }
}

