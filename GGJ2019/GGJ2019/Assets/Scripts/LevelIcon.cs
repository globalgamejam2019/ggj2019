﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is used to control each level icon.
/// </summary>
public class LevelIcon : MonoBehaviour {

    #region
    public int ID;
    #endregion

  
    public void LevelIconClicked()
    {
        ActivitySelectionManager.Instance.MakeActivityCurrent(ID);
        LevelIconManager.Instance.HighlightActiveLevel(ID);
        DeselectButton();
    }


    public void DeselectButton()
    {
        GameObject myEventSystem = GameObject.Find("EventSystem");
        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
    }
}
