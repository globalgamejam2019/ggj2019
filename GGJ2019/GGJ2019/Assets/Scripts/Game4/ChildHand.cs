﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is for managing the child's hand in Game 4.
/// </summary>
public class ChildHand : MonoBehaviour {

    #region variables
    private bool moveHand = false;
    private float handMovementSpeed = 40f;
    private float hitBackAmount = 100f;
    public bool handStopperCollided = false;
    #endregion

    void Start ()
    {
    }

   public void StartMovingHand()
    {
        moveHand = true;
    }

    public void StopMovingHand()
    {
        moveHand = false;
    }

    public void HitBackHand()
    {
        transform.Translate(Vector2.left * hitBackAmount);
    }
	
	
	void Update ()
    {
        ManageHandMovement();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "PLUGSOCKET")
        {
            Game4SaveTheChild.Instance.HandCollidedWithSocket();
        }
        
        if(collision.tag == "HANDSTOPPER")
        {
            handStopperCollided = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "HANDSTOPPER")
        {
            handStopperCollided = false;
        }
    }


    //Manages movement of the Hand
    public void ManageHandMovement()
    {
        if (!moveHand)
            return;

        transform.Translate(Vector2.right * handMovementSpeed * Time.deltaTime);
    }

    

}
