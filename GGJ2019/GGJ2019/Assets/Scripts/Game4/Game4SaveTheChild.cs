﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// This class is for managing Game 4: Saving the child
/// </summary>
public class Game4SaveTheChild : MonoBehaviour {

    #region SINGLETON PATTERN
    public static Game4SaveTheChild _instance;
    public static Game4SaveTheChild Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Game4SaveTheChild>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("Game4SaveTheChild");
                    _instance = container.AddComponent<Game4SaveTheChild>();
                }

            }
            return _instance;
        }
    }
    #endregion

    #region variables
    public ChildHand childHand;
    public GameObject slapSound;
    #endregion

    void Start ()
    {
        childHand.StartMovingHand();
    }
	
	void Update ()
    {
        ManagePlayerControls();
    }

  
    public void HandCollidedWithSocket()
    {
        //GIVE REASON FOR LOSS
        childHand.StopMovingHand();
        GameOverManager.Instance.SetGameOverSound(1);
        GameOverManager.Instance.SetGameOverReason(2);
        GameOverManager.Instance.LoadGameOver();
    }

    /// <summary>
    /// Manages the player controls for each game.
    /// </summary>
    public void ManagePlayerControls()
    {
        if(ActivitySelectionManager.Instance.currentActivity != gameObject)
        {
            return;
        }

        ManageMotherMovement();
    }

    public void ManageMotherMovement()
    {
        

        if (Input.GetKeyDown(KeyCode.Space))
        {

            if (childHand.handStopperCollided)
            {
                return;
            }
                childHand.HitBackHand();
            slapSound.GetComponent<FMODUnity.StudioEventEmitter>().Play();


        }
    }
}
