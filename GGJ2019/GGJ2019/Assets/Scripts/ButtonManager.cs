﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour {

    #region variables

    #endregion

    #region SINGLETON PATTERN
    public static ButtonManager _instance;
    public static ButtonManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<ButtonManager>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("ButtonManager");
                    _instance = container.AddComponent<ButtonManager>();
                }

            }
            return _instance;
        }
    }
    #endregion


	void Update ()
    {
        ManageTitleScreenInputs();
    }

    /// <summary>
    /// Manages the inputs received on the title screen.
    /// </summary>
    public void ManageTitleScreenInputs()
    {
        if(Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
        {
            LoadPlayScreen();
        }
    }

    public void LoadPlayScreen()
    {
        SceneManager.LoadScene("PlayScreen");
    }

    public void RestartButton()
    {
        Destroy(GameOverManager.Instance.gameObject);
        SceneManager.LoadScene("PlayScreen");
    }

    public void LoadTitleScreen()
    {
        Destroy(GameOverManager.Instance.gameObject);
        SceneManager.LoadScene("TitleScreen");
    }
}
