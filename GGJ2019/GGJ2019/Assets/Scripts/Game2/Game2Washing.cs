﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI; 

/// <summary>
/// This class is for managing Game 4: Saving the child
/// </summary>
public class Game2Washing : MonoBehaviour
{

    #region SINGLETON PATTERN
    public static Game2Washing _instance;
    public static Game2Washing Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Game2Washing>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("Game2Washing");
                    _instance = container.AddComponent<Game2Washing>();
                }

            }
            return _instance;
        }
    }
    #endregion

    #region variables
    private int levelID = 1;
    public Transform clothingSpawnPosition;
    public Transform clothingViewPosition;
    private int clothesStartingCount = 10;
    public List<GameObject> clothesList = new List<GameObject>();
    private Clothing currentPieceOfClothing;
    public List<GameObject> clothingPrefabs = new List<GameObject>();
    private int currentGameState = 0;
    public GameObject generalTimerPrefab;
    public GameObject throwClothes;
    public GameObject closeDoorSound;
    public GameObject failSound;

    public List<GameObject> washingMachine1Clothes = new List<GameObject>();
    public List<GameObject> washingMachine2Clothes = new List<GameObject>();
    public List<GameObject> washingBasketClothesSprites = new List<GameObject>();
    #region sprites
    public GameObject door1close;
    public GameObject door1open;
    public GameObject door2close;
    public GameObject door2open;
    #endregion
    public GameObject washingAnimLeft;
    public GameObject washingAnimRight;
    public GameObject leftArrowOutline;
    public GameObject rightArrowOutline;
    public GameObject currentArrowOutline;
    public List<GameObject> arrowOutlines = new List<GameObject>();


    public Text washingTextLabel;

    public List<float> washingBasketSpriteIntervals = new List<float>();
    public List<float> washingMachineClothesIntervals = new List<float>();
    private int washingBasketSpriteState;
    private float currentWashingBasketInterval;
    private float washTime = 15f;
    private int lives = 3;

    #endregion

    void Start()
    {
        SpawnNewClothes(clothesStartingCount);
        PutCurrentClothingInPosition();
        CreateWashingBasketSpriteIntervals();
        washingBasketSpriteState = washingBasketClothesSprites.Count;
    }

    void Update()
    {
        ManagePlayerControls();
    }

    public void SpawnNewClothes(int numberOfClothes)
    {
        for (int i = 0; i < numberOfClothes; i++)
        {
            GameObject newclothing = Instantiate(clothingPrefabs[Random.Range(0, clothingPrefabs.Count)]);
            newclothing.transform.position = clothingSpawnPosition.position;
            newclothing.transform.SetParent(clothingSpawnPosition);
            clothesList.Add(newclothing);
        }
    }
    
    public void PutCurrentClothingInPosition()
    {
        if (clothesList.Count == 0)
            return;
        currentPieceOfClothing = clothesList[clothesList.Count - 1].GetComponent<Clothing>();
        currentPieceOfClothing.transform.position = clothingViewPosition.position;
        currentPieceOfClothing.transform.SetParent(clothingViewPosition);
        currentPieceOfClothing.transform.localScale = new Vector3(1, 1, 1);
    }

    public void CreateWashingBasketSpriteIntervals()
    {

    }

    /// <summary>
    /// Checks to see if the activity is finished.
    /// </summary>
    public void CheckIfClothesSorted()
    {
        if(clothesList.Count == 0)
        {
            currentGameState++;
            washingTextLabel.text = "Press SPACE to close the doors!";
        }
    }

    /// <summary>
    /// Manages the player controls for each game.
    /// </summary>
    public void ManagePlayerControls()
    {
        if (ActivitySelectionManager.Instance.currentActivity != gameObject)
        {
            return;
        }

        ManageActivityMovement();
    }

    public void ManageActivityMovement()
    {
        if (currentGameState == 0)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                MoveClothingIntoMachine(0);
                OutlineArrow(0);
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                MoveClothingIntoMachine(1);
                OutlineArrow(1);
            }
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            ShutTheDoor();
        }
    }

    public void OutlineArrow(int arrow)
    {
        arrowOutlines[arrow].SetActive(true);
    }

   

    public void ShutTheDoor()
    {
        if(currentGameState == 1)
        {
            door1close.SetActive(true);
            door1open.SetActive(false);
            door2close.SetActive(true);
            door2open.SetActive(false);
            closeDoorSound.GetComponent<FMODUnity.StudioEventEmitter>().Play();
            currentGameState++;

            //START SPIN
            GameObject newTimer = Instantiate(generalTimerPrefab);
            newTimer.GetComponent<GeneralTimer>().SetupTimer(washTime, RunWashCycle);
            washingTextLabel.gameObject.SetActive(false);
            washingAnimLeft.SetActive(true);
            washingAnimRight.SetActive(true);
        }
    }

    public void RunWashCycle()
    {
        LevelIconManager.Instance.TickCompleteLevel(levelID);
        GameOverManager.Instance.ActivityCompleted();
    }

    public void MoveClothingIntoMachine(int machine)
    {
        throwClothes.GetComponent<FMODUnity.StudioEventEmitter>().Play();
        if (currentGameState > 0)
            return;
        switch (machine)
        {
            case 0:
                if (currentPieceOfClothing.isClothingDark)
                {
                    IncorrectClothingMovement(machine);
                }
                else
                {
                    CorrectClothingMovement(machine);
                }
                break;
            case 1:
                
                if (currentPieceOfClothing.isClothingDark)
                {
                    CorrectClothingMovement(machine);
                }
                else
                {
                    IncorrectClothingMovement(machine);
                }
                break;
        }
    }

    public void CorrectClothingMovement(int machine)
    {
        clothesList.Remove(currentPieceOfClothing.gameObject);
        Destroy(currentPieceOfClothing.gameObject);
        CheckIfClothesSorted();
        PutCurrentClothingInPosition();
        RemoveClothingFromWashingBasket();
    }

    public void RemoveClothingFromWashingBasket()
    {
    
        //washingBasketSpriteState--;
        //if (washingBasketSpriteState < 0)
        //    washingBasketSpriteState = 0;

        //currentWashingBasketInterval = (float)washingBasketSpriteState / (float)washingBasketClothesSprites.Count;
        float clothesPercentage = (float)clothesList.Count / (float)clothesStartingCount;
       // Debug.Log("CLOTHES %  = " + clothesPercentage + "  interval = " + currentWashingBasketInterval);
        CheckCurrentClothesPercentageAgainstIntervals(clothesPercentage);
        //if(clothesPercentage <= currentWashingBasketInterval)
        //{
        //    washingBasketClothesSprites[washingBasketSpriteState].SetActive(false);
        //}
        //clothes starting count = limit
        //clothesList.Count == current number of clothes
        //washingBasketClothesSprites.Count == number of sprites/states
        //there's 8 different sprites/states
        //for the clothing stack, you want to go downwards, removing at different points, but at an even rate
        //you need to use percentage , 7/10 = 0.7   = clothesList.Count/ clothesStartingCount will give you a percentage = 70%
        //100/washingBasketClothesSprites.Count = equal intervals = 12.5
        //every time it goes below an interval of 12.5 (87.5, 75, 62.5 etc), disable a sprite 
        //***list of intervals, everytime it hits an interval, that interval is removed from the list
        //separate function to check occurences of 2 state jumps or more at once.

        //need to check if the number is lower
    }

    public void CheckCurrentClothesPercentageAgainstIntervals(float currentPercentage)
    {
      for(int i =  0; i < washingBasketSpriteIntervals.Count; i++)
        {
            if (currentPercentage <= washingBasketSpriteIntervals[i])
            {
                washingBasketClothesSprites[i].SetActive(false);
                washingBasketSpriteIntervals.RemoveAt(i);
            }
        }

      for(int i = 0; i < washingMachineClothesIntervals.Count; i++)
        {
            if(currentPercentage < washingMachineClothesIntervals[i])
            {
                washingMachine1Clothes[i].SetActive(true);
                washingMachine2Clothes[i].SetActive(true);
                washingMachineClothesIntervals.RemoveAt(i);
            }
        }
    }

    public void IncorrectClothingMovement(int machine)
    {
        if (washingBasketSpriteState > washingBasketClothesSprites.Count)
            washingBasketSpriteState = washingBasketClothesSprites.Count;

        clothesList.Remove(currentPieceOfClothing.gameObject);
        Destroy(currentPieceOfClothing.gameObject);
        SpawnNewClothes(1);
        PutCurrentClothingInPosition();

        lives--;
        failSound.GetComponent<FMODUnity.StudioEventEmitter>().Play();
        CheckRemainingLives();
    }

    public void CheckRemainingLives()
    {
        if (lives == 0)
        {
            GameOverManager.Instance.SetGameOverReason(1);
            GameOverManager.Instance.LoadGameOver();
        }
    }

}
